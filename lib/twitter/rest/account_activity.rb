require 'twitter/rest/utils'
require 'twitter/utils'

module Twitter
      module REST
          module AccountActivity
              include Twitter::REST::Utils
              include Twitter::Utils

              def create_webhook(env_name, url)
                  perform_request(:json_post, "/1.1/account_activity/all/#{env_name}/webhooks.json?url=#{url}")
              end

              def list_webhooks(env_name)
                  perform_request(:get, "/1.1/account_activity/all/#{env_name}/webhooks.json")
              end

              #delete the webhook
              def delete_webhook(env_name, webhook_id)
                perform_request(:delete, "/1.1/account_activity/all/#{env_name}/webhooks/#{webhook_id}.json")
              end

              def trigger_crc_check(env_name, webhook_id)
                  perform_request(:json_put, "/1.1/account_activity/all/#{env_name}/webhooks/#{webhook_id}.json")
              end

              #subscribe to a webhook
              def create_subscription(env_name)
                  perform_request(:json_post, "/1.1/account_activity/all/#{env_name}/subscriptions.json")
              end

              #get subscriptions count
              #todo: use bearer token
              def get_subscriptions_count
                  perform_request(:get, "/1.1/account_activity/all/subscriptions/count.json")
              end

              #check if webhook configuration is subscribed by user
              def check_subscription(env_name)
                  perform_request(:get, "/1.1/account_activity/all/#{env_name}/subscriptions.json")
              end

              def deactivate_subscription(env_name)
                  perform_request(:delete, "/1.1/account_activity/all/#{env_name}/subscriptions.json")
              end

          end
      end
end
