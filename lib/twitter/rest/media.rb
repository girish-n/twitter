require 'twitter/rest/utils'
require 'twitter/utils'
module Twitter
  module REST
    module Media
        include Twitter::REST::Utils
        include Twitter::Utils
        
        def get_media_content(media_url)
                perform_request(:get, media_url)
        end
    end
  end
end
